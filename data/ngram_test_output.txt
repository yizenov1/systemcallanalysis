
VECTOR_LENGTH: 1000, TRAIN AVG F-SCORE: 1.00
VECTOR_LENGTH: 1000, TEST AVG F-SCORE: 0.93

VECTOR_LENGTH: 1000, TRAIN AVG F-SCORE: 1.00
VECTOR_LENGTH: 1000, TEST AVG F-SCORE: 0.93

VECTOR_LENGTH: 1000, REAL TEST F-SCORE: 0.51
 precision: 0.35, recall: 0.94
true negatives: 3704
false negatives: 21
false positives: 668
true positives: 352
 TPR: 0.94, FPR: 0.15
TNR: 0.85, FNR: 0.06
 error rate: 0.15, accuracy: 0.85

VECTOR_LENGTH: 1000, REAL TEST F-SCORE: 0.52
 precision: 0.36, recall: 0.94
true negatives: 3750
false negatives: 21
false positives: 622
true positives: 352
 TPR: 0.94, FPR: 0.14
TNR: 0.86, FNR: 0.06
 error rate: 0.14, accuracy: 0.86