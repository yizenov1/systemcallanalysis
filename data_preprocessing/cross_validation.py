import random


def data_ratio(data):

    benign_sequence = {}
    malware_sequence = {}

    for index, line in enumerate(data):
        values = line.split(',')
        if values[0] == "1":
            malware_sequence[index] = (values[0], values[1].lower())
        else:
            benign_sequence[index] = (values[0], values[1].lower())

    benign_size, malware_size = len(benign_sequence), len(malware_sequence)
    if benign_size > malware_size:
        return [benign_sequence, malware_sequence, True]
    return [benign_sequence, malware_sequence, False]


def k_fold_cv(benign_data, malware_data, k):

    k_fold_data = []
    for idk in range(0, k):
        k_fold_data.append([])

    benign_size = len(benign_data) / k
    malware_size = len(malware_data) / k

    # TODO: make selection random. Is there big difference?
    index, idk = 0, 0
    for item in benign_data:
        if index < benign_size:
            k_fold_data[idk].append((item, benign_data[item]))
            index += 1
        else:
            idk += 1
            index = 0

    index, idk = 0, 0
    for item in malware_data:
        if index < malware_size:
            k_fold_data[idk].append((item, malware_data[item]))
            index += 1
        else:
            idk += 1
            index = 0
    return k_fold_data


def join_data(data_array, number):
    joint_train_data = []
    for idx in range(0, len(data_array)):
        if idx != number:
            joint_train_data.extend(data_array[idx])
    return joint_train_data
