import os


def read_file(file_path):
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    input_file = open(cur_path + file_path, "r")
    return input_file


def output_file(file_path):
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    file_name = open(cur_path + file_path, "w")
    return file_name
