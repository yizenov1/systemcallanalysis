import numpy as np
from operator import itemgetter
import os
import seaborn as sns
import matplotlib.pyplot as plt

# IS THERE A FRAMEWORK TO DO THIS ALL?

# number of sequences
# number of BC and MC

# number and list of unique systems calls
# average, max and min length of sequences

# frequencies of unique system call in all corpus and in each sequence
# neighbor frequencies (post-pre order)


def start_statistics(data, output_file, plots_folder):

    counter, missing_lines = number_of_sequences(data)
    # print("\nNumber of sequences:", counter)
    # print("Number of empty sequences:", missing_lines)
    output_file.write("\nNumber of sequences: " + str(counter) + "\n")
    output_file.write("Number of empty sequences: " + str(missing_lines) + "\n")

    benign, malicious = ratio_of_classes(data)
    # print("\nNumber of benign sequences", benign)
    # print("Number of malicious sequences", malicious)
    output_file.write("\nNumber of benign sequences: " + str(benign) + "\n")
    output_file.write("Number of malicious sequences: " + str(malicious) + "\n")

    average, maximum, minimum, distribution = lengths_of_sequences(data)
    # print("\nLengths of sequences: average %s, maximum %s, minimum %s" % (average, maximum, minimum))
    output_file.write("\nLengths of sequences: average " + str(average) + ", maximum " + str(maximum) +
                      ", minimum " + str(minimum) + "\n")
    plot(distribution, "sequence_lengths", plots_folder)  # list(range(len(distribution)))

    # print("\nNumber of uniques system calls %s\n" % number_of_unique_calls(data))
    output_file.write("\nNumber of uniques system calls: " + str(number_of_unique_calls(data)) + "\n")

    sorted_by_frequency = unique_calls_frequencies(data)
    # print(*sorted_by_frequency, sep='\n')
    output_file.write("\nFrequencies of system calls:\n")
    for item in sorted_by_frequency:
        output_file.write(str(item) + " average: " + str(item[1] / counter))
        output_file.write("\n")
    # plot(list(sorted_by_frequency.values()), "system_call_frequency", plots_folder)  # TODO:


def number_of_sequences(data):
    counter, missing_lines = 0, 0
    for line in data:
        line = line.strip()
        if not line:
            missing_lines += 1
        else:
            counter += 1
    data.seek(0)
    return counter, missing_lines


def ratio_of_classes(data):
    benign, malicious = 0, 0
    for line in data:
        values = line.split(',')
        if values[0] == '0':
            benign += 1
        else:
            malicious += 1
    data.seek(0)
    return benign, malicious


def list_of_unique_calls(data):
    unique_calls_list = set()
    for line in data:
        values = line.split(',')
        calls = values[1].lower().split(' ')
        for call in calls:
            if call not in unique_calls_list:
                unique_calls_list.add(call)
    data.seek(0)
    return unique_calls_list


def number_of_unique_calls(data):
    unique_calls_list = set()
    for line in data:
        values = line.split(',')
        calls = values[1].lower().split(' ')
        for call in calls:
            if call not in unique_calls_list:
                unique_calls_list.add(call)
    data.seek(0)
    return len(unique_calls_list)


def lengths_of_sequences(data):
    lengths = []
    for line in data:
        values = line.split(',')
        calls = values[1].split(' ')
        lengths.append(len(calls))
    data.seek(0)
    return int(np.mean(lengths)), np.amax(lengths), np.amin(lengths), lengths


def unique_calls_frequencies(data):
    unique_calls_map = {}
    for line in data:
        values = line.split(',')
        calls = values[1].lower().split(' ')
        for call in calls:
            if call not in unique_calls_map:
                unique_calls_map[call] = 0
            else:
                unique_calls_map[call] += 1
    data.seek(0)
    return sorted(unique_calls_map.items(), key=itemgetter(1), reverse=True)


def plot(distribution, name, folder):
    # TODO: titles for x and y axis
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    abs_folder_path = str(cur_path) + folder
    sns.set_style('whitegrid')

    plt.title('%s frequency distribution' % name)
    # plt.axis([-200, 200, 0, 180])
    plt.hist(distribution)
    # plt.hist2d(x_distribution, y_distribution)
    plt.savefig(abs_folder_path + "%s.png" % name)
