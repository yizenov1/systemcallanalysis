import os
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, isdir, join
import numpy as np
from operator import itemgetter
from random import randint
from sklearn.metrics import f1_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_score
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn import linear_model
from sklearn.base import clone
from itertools import chain
import math

from ngram_based import ngram_collection as ng
from graph_based import graph_modeling as gm
from performance_measuring import accuracy_metrics


def read_parent_folder(folder_path):
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    path = cur_path + folder_path
    sub_folders = [folder_path + f for f in listdir(path) if isdir(join(path, f))]
    return sub_folders, path


def read_folder(folder_path):
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    path = cur_path + folder_path
    files = [f for f in listdir(path) if isfile(join(path, f))]
    return files, path


def read_file(file_path):
    input_file = open(file_path, "r")
    return input_file


def write_to_file(files, path, output_file):
    for input_file in files:
        opened_file = read_file(path + input_file)
        for line in opened_file:
            output_file.write(line + "\n")


def output_to_file(objects, output_file):
    for item in objects:
        output_file.write(item[1])


def list_of_unique_calls(data):
    unique_calls_list = set()
    for line in data:
        calls = line.split(' ')
        for call in calls:
            if call not in unique_calls_list:
                unique_calls_list.add(call)
    data.seek(0)
    return unique_calls_list


def number_of_unique_calls(data):
    unique_calls_list = set()
    for line in data:
        calls = line.split(' ')
        for call in calls:
            if call not in unique_calls_list:
                unique_calls_list.add(call)
    data.seek(0)
    return len(unique_calls_list)


def lengths_of_sequences(data):
    lengths = []
    for line in data:
        calls = line.split(' ')
        lengths.append(len(calls))
    data.seek(0)
    return int(np.mean(lengths)), np.amax(lengths), np.amin(lengths)


def unique_calls_frequencies(sequences):
    unique_calls_map = {}
    for line in sequences:
        calls = line[1].split()
        for call in calls:
            if call not in unique_calls_map:
                unique_calls_map[call] = 1
            else:
                unique_calls_map[call] += 1
    return sorted(unique_calls_map.items(), key=itemgetter(1), reverse=True)


def extract_stat(benign_data, malware_data, output_file, labels_file):

    for index, data in enumerate([benign_data, malware_data]):

        lengths_of_sequence = lengths_of_sequences(data)
        number_of_calls = number_of_unique_calls(data)
        # unique_calls_frequency = unique_calls_frequencies(data)

        output_file.write("\nNumber of unique calls of class '%s': %i\n" % (index, number_of_calls))
        output_file.write("Avg length: %i, max length: %i, min length: %i\n" % (lengths_of_sequence[0], lengths_of_sequence[1], lengths_of_sequence[2]))

    unique_system_calls1, unique_system_calls2 = list_of_unique_calls(benign_data), list_of_unique_calls(malware_data)
    unique_system_calls = set(chain(unique_system_calls1, unique_system_calls2))
    output_file.write("\nTotal number of unique system calls: %i\n" % len(unique_system_calls))

    # aa = pd.read_csv(benign_input_file.name, sep=' ', header=None)  # TODO: 26th line too long
    sequences1, sequences2 = data_to_array(benign_data, 0), data_to_array(malware_data, 1)
    sequences = np.array([*sequences1, *sequences2])
    output_file.write("Total number of sequences: %i\n" % len(sequences))
    output_file.write("Ratio of benign and malware sequences: %i vs %i\n" % (len(sequences1), len(sequences2)))

    benign_frequencies, malware_frequencies = unique_calls_frequencies(sequences1), unique_calls_frequencies(sequences2)
    all_frequencies = unique_calls_frequencies(sequences)

    """labels = {}
    for line in labels_file:
        values = line.split(' ')
        labels[values[0]] = values[1][:len(values[1])-1]

    for call in all_frequencies:
        if call[0] in labels:
            print(labels[call[0]] + " " + str(call[1]))
    labels_file.seek(0)"""

    plot_frequencies(benign_frequencies, "benign_frequencies")
    plot_frequencies(malware_frequencies, "malware_frequencies")
    plot_frequencies(all_frequencies, "all_frequencies")

    return unique_system_calls, sequences


def plot_frequencies(data, file_name):

    keys, values = [], []
    for item in data:
        keys.append(item[0])
        values.append(item[1])

    folder_path = "\\plots\\"
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    full_folder_path = cur_path + folder_path

    plt.scatter(keys, values).get_figure()
    plt.xlabel('unique system calls')
    plt.ylabel('frequency')
    file_name = full_folder_path + str(file_name) + ".png"
    try:
        os.remove(file_name)
    except OSError:
        pass
    plt.savefig(file_name)
    plt.close()


def data_to_array(data, label):
    sequences = []
    for line in data:
        sequences.append((label, line))
    data.seek(0)
    return np.array(sequences)


def data_with_indices(sequences, indices):
    objects = []
    for index in indices:
        objects.append((index, sequences[index]))
    return objects


# The model needs to be trained by all data
def run_experiment(sequences, kf, models, ngram_container_size, vector_length, ngram_length, ngram_output):

    k, accuracies = float(kf.get_n_splits(sequences)), {}

    for train_indices, test_indices in kf.split(sequences):  # ratio is 20%

        train_data, test_data = sequences[train_indices], sequences[test_indices]

        # selected_features = ng.build_class_vectors(train_data, ngram_length, vector_length, ngram_container_size)
        selected_features = ng.build_class_vectors_with_tf_idf(train_data, ngram_length, vector_length)

        selected_features_with_weights = {}
        weights = ng.all_unique_idfs(train_data, ngram_length)
        for feature in selected_features:
            item = weights[feature[0]]
            selected_features_with_weights[feature[0]] = item[1]

        # parameters = [selected_features, ngram_length, vector_length, ngram_container_size]
        parameters = [selected_features_with_weights, ngram_length, vector_length, ngram_container_size]

        # train_feature_vectors = create_feature_vectors(sequences, train_indices, parameters)
        train_feature_vectors = create_feature_vectors_with_idf(sequences, train_indices, parameters)  # works correct
        # test_feature_vectors = create_feature_vectors(sequences, test_indices, parameters)
        test_feature_vectors = create_feature_vectors_with_idf(sequences, test_indices, parameters)

        for model_idx, model in enumerate(models):

            new_model = clone(model)  # gets default instance of the model
            train_model(train_feature_vectors, new_model)

            train_answers, train_predictions, predicted_probabilities = classify_test_data(train_feature_vectors, new_model)
            test_answers, test_predictions, predicted_probabilities = classify_test_data(test_feature_vectors, new_model)

            # can output other details such FPR, FNR, etc.
            train_accuracy = f1_score(train_answers, train_predictions)  # this works correct
            test_accuracy = f1_score(test_answers, test_predictions)

            # ngram_output.write("\n model_type: %i, TRAIN F-SCORE: %f\n" % (model_idx, train_accuracy))
            # ngram_output.write("\n model_type: %i, TEST F-SCORE: %f\n" % (model_idx, test_accuracy))

            # TODO: is it good to consider the average of k-fold CV results?
            if model_idx not in accuracies:
                accuracies[model_idx] = (0, 0)
            accuracy_tuple = accuracies[model_idx]
            accuracies[model_idx] = (accuracy_tuple[0]+train_accuracy, accuracy_tuple[1]+test_accuracy)

    for model_idx in accuracies:
        avg_train_accuracy = accuracies[model_idx][0] / k
        avg_test_accuracy = accuracies[model_idx][1] / k
        ngram_output.write("\nVECTOR_LENGTH: %i, TRAIN AVG F-SCORE: %0.02f\n" % (vector_length, avg_train_accuracy))
        ngram_output.write("VECTOR_LENGTH: %i, TEST AVG F-SCORE: %0.02f\n" % (vector_length, avg_test_accuracy))


def test_on_new_data(train_sequences, test_sequences, models, ngram_container_size, vector_length, ngram_length, ngram_output, labels):
    # THIS IS A TEST STAGE
    # TODO: skip first 10
    # selected_features = ng.build_class_vectors(train_sequences, ngram_length, vector_length, ngram_container_size)
    selected_features = ng.build_class_vectors_with_tf_idf(train_sequences, ngram_length, vector_length)

    # extract_labels(selected_features, ngram_length, labels)

    selected_features_with_weights = {}
    weights = ng.all_unique_idfs(train_sequences, ngram_length)
    for feature in selected_features:
        item = weights[feature[0]]
        selected_features_with_weights[feature[0]] = item[1]

    # parameters = [selected_features, ngram_length, vector_length, ngram_container_size]
    parameters = [selected_features_with_weights, ngram_length, vector_length, ngram_container_size]

    train_indices, test_indices = range(len(train_sequences)), range(len(test_sequences))
    # train_feature_vectors = create_feature_vectors(train_sequences, train_indices, parameters)  # SPARSE
    train_feature_vectors = create_feature_vectors_with_idf(train_sequences, train_indices, parameters)
    # test_feature_vectors = create_feature_vectors(test_sequences, test_indices, parameters)  # SPARSE
    test_feature_vectors = create_feature_vectors_with_idf(test_sequences, test_indices, parameters)

    model_accuracy = 0
    for index, next_model in enumerate(models):

        # training model
        model = clone(next_model)
        train_model(train_feature_vectors, model)

        # testing
        test_answers, test_predictions, predicted_probabilities = classify_test_data(test_feature_vectors, model)

        test_accuracy = f1_score(test_answers, test_predictions)
        ngram_output.write("\nVECTOR_LENGTH: %i, REAL TEST F-SCORE: %0.02f" % (vector_length, test_accuracy))
        model_accuracy = test_accuracy

        # TPR(recall, Sensitivity) = TP/(TP+FN)   FPR = FP/(FP+TN)
        # TPR = correct classified malware / (wrong classified malware + correct classified malware)
        # FPR = wrong classified benign / (wrong classified benign + correct classified benign)
        fpr, tpr, thresholds = roc_curve(test_answers, predicted_probabilities[:, 1])  # positive labels only
        # predicted_probabilities[:, 1] - probability estimates that a sample is more likely to be malware
        # column0 is positive, column2 is negative
        # if column1 is bigger than column0, then the sample is malware
        auc_score = roc_auc_score(np.array(test_answers), np.array(predicted_probabilities[:, 1]))
        accuracy_metrics.roc_auc_plot(fpr, tpr, auc_score, (1 + index))

        precision, recall, thresholds = precision_recall_curve(test_answers, predicted_probabilities[:, 1])
        avg_pr_score = average_precision_score(test_answers, predicted_probabilities[:, 1])
        accuracy_metrics.pr_score_plot(precision, recall, avg_pr_score, (1 + index))

        # TP/(TP+FP)
        # correct classified malware / (correct classified malware + wrong classified benign)
        p_score = precision_score(test_answers, test_predictions)
        r_score = recall_score(test_answers, test_predictions)
        ngram_output.write("\n precision: %0.02f, recall: %0.02f" % (p_score, r_score))

        # tn, fp, fn, tp
        tn, fp, fn, tp = confusion_matrix(test_answers, test_predictions).ravel()
        ngram_output.write("\ntrue negatives: " + str(tn) + "\nfalse negatives: " + str(fn))
        ngram_output.write("\nfalse positives: " + str(fp) + "\ntrue positives: " + str(tp))

        false_positive_rate = fp / float(fp + tn)
        ngram_output.write("\n TPR: %0.02f, FPR: %0.02f" % (r_score, false_positive_rate))

        # True negative rate (Specificity) and False negative rate
        tnr, fnr = tn / (tn + fp), fn / (tp + fn)
        ngram_output.write("\nTNR: %0.02f, FNR: %0.02f" % (tnr, fnr))

        # Overall accuracy
        error_rate = (fn + fp) / float(tn + tp + fn + fp)
        ngram_output.write("\n error rate: %0.02f, accuracy: %0.02f" % (error_rate, 1-error_rate))

        """for i in range(len(test_answers)):
            print(str(test_answers[i]) + " " + str(test_predictions[i]) + " " + str(predicted_probabilities[i]))"""

    return model_accuracy


def create_feature_vectors(sequences, train_indices, parameters):

    # THIS SEPARATION FOR MODEL TRAINING
    benign_vectors, malware_vectors = {}, {}

    for index in train_indices:
        item = sequences[index]
        if item[0] == "1":
            malware_vectors[index] = item[1]
        else:
            benign_vectors[index] = item[1]

    benign_feature_vectors, malware_feature_vectors = {}, {}

    for index in benign_vectors:

        local_ngrams, local_vector = ng.build_ngrams(benign_vectors[index], parameters[1]), []

        for selected_ngram in parameters[0]:

            if selected_ngram[0] in local_ngrams:
                local_vector.append(math.log(1+local_ngrams[selected_ngram[0]]))
            else:
                local_vector.append(math.log(1+0))

        benign_feature_vectors[index] = local_vector

    for index in malware_vectors:

        local_ngrams, local_vector = ng.build_ngrams(malware_vectors[index], parameters[1]), []

        for selected_ngram in parameters[0]:

            if selected_ngram[0] in local_ngrams:
                local_vector.append(math.log(local_ngrams[selected_ngram[0]]))
            else:
                local_vector.append(math.log(1+0))

        malware_feature_vectors[index] = local_vector

    # return {**benign_feature_vectors, **malware_feature_vectors}
    return [benign_feature_vectors, malware_feature_vectors]


def create_feature_vectors_with_idf(sequences, train_indices, parameters):

    # THIS SEPARATION FOR MODEL TRAINING
    benign_vectors, malware_vectors = {}, {}

    for index in train_indices:
        item = sequences[index]
        if item[0] == "1":
            malware_vectors[index] = item[1]
        else:
            benign_vectors[index] = item[1]

    benign_feature_vectors, malware_feature_vectors = {}, {}

    for index in benign_vectors:

        local_ngrams = ng.build_ngrams(benign_vectors[index], parameters[1])
        local_vector = []

        selected_ngrams = parameters[0]
        for selected_ngram in selected_ngrams:
            idf = selected_ngrams[selected_ngram]
            if selected_ngram in local_ngrams:
                local_vector.append(math.log(1 + local_ngrams[selected_ngram]) * idf)  # TODO: doesn't debug
                #local_vector.append(math.log(2 + local_ngrams[selected_ngram]) * idf)
                # local_vector.append((1 + math.log(local_ngrams[selected_ngram])) * idf)  # TODO: sublinear
                # local_vector.append(local_ngrams[selected_ngram] * idf)
            else:
                local_vector.append(math.log(1 + 0.0) * idf)
                #local_vector.append(math.log(2 + 0.0) * idf)
                # local_vector.append(idf)
                # local_vector.append(0.0)

        benign_feature_vectors[index] = local_vector

    for index in malware_vectors:

        local_ngrams = ng.build_ngrams(malware_vectors[index], parameters[1])
        local_vector = []

        selected_ngrams = parameters[0]
        for selected_ngram in selected_ngrams:
            idf = selected_ngrams[selected_ngram]
            if selected_ngram in local_ngrams:
                local_vector.append(math.log(1 + local_ngrams[selected_ngram]) * idf)
                #local_vector.append(math.log(2 + local_ngrams[selected_ngram]) * idf)
                # local_vector.append((1 + math.log(local_ngrams[selected_ngram])) * idf)
                # local_vector.append(local_ngrams[selected_ngram] * idf)
            else:
                local_vector.append(math.log(1 + 0.0) * idf)
                #local_vector.append(math.log(2 + 0.0) * idf)
                # local_vector.append(idf)
                # local_vector.append(0.0)

        malware_feature_vectors[index] = local_vector

    # return {**benign_feature_vectors, **malware_feature_vectors}
    return [benign_feature_vectors, malware_feature_vectors]


def train_model(train_feature_vectors, model):

    train_x, train_y = [], []

    # benign data
    for index in train_feature_vectors[0]:
        feature_vector = train_feature_vectors[0][index]
        train_x.append(feature_vector)
        train_y.append(0)

    # malware data
    for index in train_feature_vectors[1]:
        feature_vector = train_feature_vectors[1][index]
        train_x.append(feature_vector)
        train_y.append(1)

    model.fit(train_x, train_y)


def classify_test_data(test_feature_vectors, model):

    true_labels, test_x = [], []

    # benign data
    for index in test_feature_vectors[0]:
        feature_vector = test_feature_vectors[0][index]
        test_x.append(feature_vector)
        true_labels.append(0)

    # malware data
    for index in test_feature_vectors[1]:
        feature_vector = test_feature_vectors[1][index]
        test_x.append(feature_vector)
        true_labels.append(1)

    predicted_labels = model.predict(test_x)  # this can be probability or class labels
    predicted_probabilities = model.predict_proba(test_x)
    return true_labels, predicted_labels, predicted_probabilities


def extract_labels(selected_feature, ngram_length, label_file):

    labels = {}
    for line in label_file:
        values = line.split(' ')
        labels[int(values[0])] = values[1][:len(values[1])-1]

    labeled_ngrams = {}
    for feature in selected_feature:
        ngram, temp = feature[0], []
        for idx in range(ngram_length):
            if ngram[idx] in labels:
                temp.append(labels[ngram[idx]])
            else:
                print("missing id: " + str(ngram[idx]))
        labeled_ngrams[ngram] = temp

    for ngram in labeled_ngrams:
        print(str(ngram) + " " + str(labeled_ngrams[ngram]))


def output_features(train_data, test_data, feature_file, feature_output_file):

    labels = {}
    for line in feature_file:
        values = line.split(' ')
        labels[values[0]] = values[1]

    unique_idx, uniques_labels = set(), {}
    [unique_idx.add(idx) for idx in train_data]
    [unique_idx.add(idx) for idx in test_data]

    for idx in unique_idx:
        if idx in labels:
            uniques_labels[idx] = labels[idx]
        else:
            # print(idx)
            uniques_labels[idx] = "NOT_FOUND\n"  # TODO: find them and compare in .h file in ubuntu kernel source code

    [feature_output_file.write(str(i) + " " + uniques_labels[i]) for i in uniques_labels]


def get_labels(sys_labels_file, output_file):
    labels = {}

    lines = sys_labels_file.read().splitlines()
    for i in range(len(lines)):
        line = lines[i]
        values = line.split(' ')
        if len(values) == 3 and values[0] == "#define":
            next_line = lines[i + 1]
            splits = next_line.split(' ')

            if len(splits) == 2:
                label, p_number = splits[1][:len(splits[1])-1], values[2]
                if p_number not in labels:
                    labels[p_number] = label
            elif len(splits) == 3:
                label, p_number = splits[2][:len(splits[2])-1], values[2]
                if p_number not in labels:
                    labels[p_number] = label

    [output_file.write(str(i) + " " + labels[i] + "\n") for i in labels]

    # #define __NR_rt_sigsuspend 133, 220, 221
    # __SYSCALL(__NR_rt_sigsuspend, sys_rt_sigsuspend) /* __ARCH_WANT_SYS_RT_SIGSUSPEND */
