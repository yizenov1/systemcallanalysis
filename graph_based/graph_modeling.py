import networkx as nx
import numpy as np
import scipy.stats as st
import sys
from sklearn import cross_validation
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score

from graph_based import graph_similarity
import data_preprocessing.cross_validation as cv


def initial_graph(unique_system_calls):
    graph = nx.DiGraph()
    for system_call in unique_system_calls:
        graph.add_node(system_call)
    return graph


def create_graphs(data, unique_system_calls):
    graphs = {}
    for item in data:
        line_idx = item[0]
        values = item[1]
        system_calls = values[1].lower().split(' ')
        graph = graphs_from_systems_calls(system_calls, unique_system_calls)

        for u, v, weight in graph.edges(data='weight'):
            print(str(u) + " " + str(v) + " " + str(weight))

        graph_eigenvector = graph_similarity.graph_eigenvector(graph)
        """if graph_eigenvector is None:  # TODO: fixing convergence
            continue"""
        graphs[line_idx] = (values[0], graph_eigenvector)
    return graphs


def graphs_from_systems_calls(system_calls, unique_system_calls):

    # directed, ordered(by time)
    # connected (graph per sequence)
    # self loops are included
    # multiple edges = weights

    directed_graph = initial_graph(unique_system_calls)
    previous_sys_call = None

    for system_call in system_calls:
        if previous_sys_call is not None:
            if directed_graph.has_edge(previous_sys_call, system_call):
                weight = directed_graph[previous_sys_call][system_call]['weight']
                directed_graph[previous_sys_call][system_call]['weight'] = weight + 1
            else:
                directed_graph.add_weighted_edges_from([(previous_sys_call, system_call, 1)])
        previous_sys_call = system_call

    return directed_graph


def classify_test_data(test_data, graph_eigenvectors, unique_system_calls):

    predicted_labels = []
    true_labels = []

    for item in test_data:
        # line_index = item[0]
        values = item[1]
        system_calls = values[1].lower().split(' ')

        test_graph = graphs_from_systems_calls(system_calls, unique_system_calls)
        test_eigenvector = graph_similarity.graph_eigenvector(test_graph)
        """if test_eigenvector is None:  # TODO: fixing convergence
            continue"""
        true_labels.append(int(values[0]))
        # test_eigenvectors.append(test_eigenvector)

        min_distance = sys.maxsize  # maxint
        test_label = 0

        for idx in graph_eigenvectors:
            item = graph_eigenvectors[idx]
            temp = graph_similarity.compare_graphs(item[1], test_eigenvector)

            if temp < min_distance:
                min_distance = temp
                test_label = item[0]

        predicted_labels.append(int(test_label))

    """test_eigenvectors = []
    train_labels = []
    train_eigenvectors = []
    for line_idx in graph_eigenvectors:
        item = graph_eigenvectors[line_idx]
        train_labels.append(item[0])
        train_eigenvectors.append(item[1])
    shaped_labels = np.asarray(train_labels).T
    shaped_vectors = np.asarray(train_eigenvectors).T
    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(shaped_vectors, shaped_labels)  # TODO: sizes are different
    predicted_labels = list(knn.predict(test_eigenvectors))"""

    return [true_labels, predicted_labels]


def run_experiment(cv_data, unique_system_calls):

    avg_test_f1_score, avg_train_f1_score = 0, 0
    avg_test_precision_score, avg_test_recall_score = 0, 0
    avg_train_precision_score, avg_train_recall_score = 0, 0

    for counter in range(0, len(cv_data)):

        train_data = cv.join_data(cv_data, counter)
        test_data = cv_data[counter]

        graph_eigenvectors = create_graphs(train_data, unique_system_calls)

        answers, predictions = classify_test_data(test_data, graph_eigenvectors, unique_system_calls)
        avg_test_f1_score += f1_score(answers, predictions)
        avg_test_precision_score += precision_score(answers, predictions)
        avg_test_recall_score += recall_score(answers, predictions)

        # plot_pc_curve(answers, predictions)  # TODO:

        answers, predictions = classify_test_data(train_data, graph_eigenvectors, unique_system_calls)
        avg_train_f1_score += f1_score(answers, predictions)
        avg_train_precision_score += precision_score(answers, predictions)
        avg_train_recall_score += recall_score(answers, predictions)

    avg_test_f1_score /= float(len(cv_data))
    avg_train_f1_score /= float(len(cv_data))
    avg_test_precision_score /= float(len(cv_data))
    avg_test_recall_score /= float(len(cv_data))
    avg_train_precision_score /= float(len(cv_data))
    avg_train_recall_score /= float(len(cv_data))

    print("\nAvg test f-score, precision, recall: %f, %f, %f"
          % (avg_test_f1_score, avg_test_precision_score, avg_test_recall_score))
    print("Avg train f-score, precision, recall: %f, %f, %f\n"
          % (avg_train_f1_score, avg_train_precision_score, avg_train_recall_score))


# Precision, Recall, ROC, F-SCORE
def plot_pc_curve(y_test, y_score):
    precision, recall, _ = precision_recall_curve(y_test, y_score)
    average_precision = average_precision_score(y_test, y_score)

    plt.step(recall, precision, color='b', alpha=0.2, where='post')
    plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('2-class Precision-Recall curve: AUC={0:0.2f}'.format(average_precision))


# finding threshold via statistics(p-value, confidence interval, etc.)
# this is only a sample data, TODO: plot the distribution
def detect_normal_similarity_thresholds(graphs, confidence_interval):

    # benign_thresholds = module4.detect_normal_similarity_thresholds(graph_models[0], 0.95)  # TODO: is this bias?
    # malware_thresholds = module4.detect_normal_similarity_thresholds(graph_models[1], 0.95)

    similarities = []
    indices = set()
    for graph in graphs:
        indices.add(graph)
        for next_graph in graphs:
            if next_graph not in indices:
                similarities.append(graph_similarity.compare_graphs(graphs[graph], graphs[next_graph]))

    size, mean, sigma = len(similarities) - 1, np.mean(similarities), np.std(similarities)
    val1, val2 = st.t.interval(confidence_interval, size, loc=mean, scale=sigma)
    return [val1, val2]
