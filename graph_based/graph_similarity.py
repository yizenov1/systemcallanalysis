import networkx as nx
from numpy.linalg import eigvals
import numpy as np
from scipy.spatial.distance import euclidean


def graph_eigenvector(di_graph):

    directed_laplacian = nx.directed_laplacian_matrix(di_graph)
    eigenvalues, eigenvectors = np.linalg.eigh(directed_laplacian)

    sorted_eigenvalues = sorted(eigenvalues)
    first_min, second_min = sorted_eigenvalues[0], sorted_eigenvalues[1]

    first_min_idx = [index for index, value in enumerate(eigenvalues) if value == first_min]
    second_min_idx = [index for index, value in enumerate(eigenvalues) if value == second_min]

    first_vector = eigenvectors[:, first_min_idx]
    second_vector = eigenvectors[:, second_min_idx]

    transpose = np.asarray(first_vector).T
    return transpose[0]


def compare_graphs(graph1_vector, graph2_vector):

    """# when sizes are different
    k1 = select_k(graph1_vector)
    k2 = select_k(graph2_vector)
    k = min(k1, k2)
    similarity = sum((graph1_vector[:k] - graph2_vector[:k]) ** 2)  # not square-rooted
    return float(similarity)  # TODO: complex values"""

    distance = euclidean(graph1_vector, graph2_vector)
    return distance


def select_k(spectrum, minimum_energy=0.9):
    running_total = 0.0
    total = sum(spectrum)
    if total == 0.0:
        return len(spectrum)
    for i in range(len(spectrum)):
        running_total += spectrum[i]
        if running_total / total >= minimum_energy:
            return i + 1
    return len(spectrum)
