import data_preprocessing.data_loading as module1
from data_preprocessing import data_statistics
import ngram_based.ngram_collection as module2
from graph_based import graph_modeling
import data_preprocessing.cross_validation as module6
import data_preprocessing.run_new_data as module7
from performance_measuring import accuracy_metrics as module8

from itertools import chain
import math
from sklearn.model_selection import KFold
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn import linear_model
import numpy as np
import pandas as pd
from sklearn.metrics import f1_score
from sklearn.base import clone
from sklearn.ensemble import RandomForestClassifier
import nltk

########################################################################################

TRAIN_FILE = "/data/CSDMC_API_Train.csv"
TEST_FILE = "/data/CSDMC_API_TestData.csv"

PLOTS_FOLDER = "/plots/"

########################################################################################

NEW_TRAIN_FOLDER = "/DATASETS/ADFA-IDS_DATASETS/ADFA-LD/Training_Data_Master/"
NEW_TEST_FOLDER = "/DATASETS/ADFA-IDS_DATASETS/ADFA-LD/Validation_Data_Master/"
NEW_ATTACK_FOLDER = "/DATASETS/ADFA-IDS_DATASETS/ADFA-LD/Attack_Data_Master/"
SYS_LABELS_FILE = "\\data\\ADFA-LDSyscallList.txt"

OUTPUT_TRAIN_FILE = "/data/normal_train_data.txt"
OUTPUT_TEST_FILE = "/data/normal_test_data.txt"
SHUFFLED_OUTPUT_TRAIN_FILE = "/data/shuffled_normal_train_data.txt"
SHUFFLED_OUTPUT_TEST_FILE = "/data/shuffled_normal_test_data.txt"

# attack data is shuffled
OUTPUT_ATTACK_FILE = "/data/attack_data.txt"
TRAIN_ATTACK_FILE = "/data/train_attack_data.txt"
TEST_ATTACK_FILE = "/data/test_attack_data.txt"

STAT_FILE = "/data/statistics.txt"
SYS_CALLS_LABELS = "/data/sys_labels.txt"
EXTRACTED_SYS_CALL_LABELS = "/data/extracted_sys_call_labels.txt"
NGRAM_TEST_OUTPUT = "/data/ngram_test_output.txt"

########################################################################################

"""train_data_file = module1.read_file(TRAIN_FILE)
unique_system_calls = data_statistics.list_of_unique_calls(train_data_file)"""

########################################################################################

"""K_FOLD_CV, NGRAM_LENGTH, VECTOR_LENGTH, NGRAM_CONTAINER_SIZE = 5, 3, 1000, 10000
data_ratio = module6.data_ratio(train_data_file)
ngram_output = module1.output_file(NGRAM_TEST_OUTPUT)
ngram_output.write("Benign: %i, Malware: %i\n\n" % (len(data_ratio[0]), len(data_ratio[1])))
cv_data = module6.k_fold_cv(data_ratio[0], data_ratio[1], K_FOLD_CV)"""

########################################################################################

# module2.run_experiment(cv_data, NGRAM_CONTAINER_SIZE, VECTOR_LENGTH, NGRAM_LENGTH, ngram_output)

########################################################################################

# graph_modeling.run_experiment(cv_data, unique_system_calls)

########################################################################################
# THIS IS REWRITING INTO SINGLE FILE
"""files, path = module7.read_folder(NEW_TEST_FOLDER)
output_file = module1.output_file(OUTPUT_TEST_FILE)
module7.write_to_file(files, path, output_file)

files, path = module7.read_folder(NEW_TRAIN_FOLDER)
output_file = module1.output_file(OUTPUT_TRAIN_FILE)
module7.write_to_file(files, path, output_file)

folders, path = module7.read_parent_folder(NEW_ATTACK_FOLDER)
output_file = module1.output_file(OUTPUT_ATTACK_FILE)
for folder in folders:
    files, path = module7.read_folder(folder)
    module7.write_to_file(files, path + '/', output_file)"""

########################################################################################
# THIS IS CREATING AND SHUFFLING TRAIN AND TEST NORMAL DATA
"""benign_input_file = module1.read_file(OUTPUT_TEST_FILE)  # OUTPUT_TRAIN_FILE
all_normal_data = module7.data_to_array(benign_input_file, 0)
kf = KFold(n_splits=2, shuffle=True)
for train_index, test_index in kf.split(all_normal_data):  # this shuffles correct
    train_normal, test_normal = all_normal_data[train_index], all_normal_data[test_index]
    all_normal = chain(train_normal, test_normal)
    output_file = module1.output_file(SHUFFLED_OUTPUT_TEST_FILE)  # SHUFFLED_OUTPUT_TRAIN_FILE
    module7.output_to_file(all_normal, output_file)
    break"""
########################################################################################
# THIS IS CREATING AND SHUFFLING TRAIN AND TEST ATTACK DATA
"""malware_input_file = module1.read_file(OUTPUT_ATTACK_FILE)
all_attack_data = module7.data_to_array(malware_input_file, 1)
kf = KFold(n_splits=2, shuffle=True)
for train_index, test_index in kf.split(all_attack_data):  # this shuffles correct
    train_attack, test_attack = all_attack_data[train_index], all_attack_data[test_index]
    output_file = module1.output_file(TRAIN_ATTACK_FILE)
    module7.output_to_file(train_attack, output_file)
    output_file = module1.output_file(TEST_ATTACK_FILE)
    module7.output_to_file(test_attack, output_file)
    break"""
########################################################################################
# THIS IS PRE-PROCESSING AND STATISTIC ANALYSIS
stat_output = module1.output_file(STAT_FILE)

labels = module1.read_file(EXTRACTED_SYS_CALL_LABELS)

# can use all attacks since it uses cross-validation
# TODO: shuffle attack data
benign_input_file, malware_input_file = module1.read_file(OUTPUT_TRAIN_FILE), module1.read_file(TRAIN_ATTACK_FILE)
unique_system_calls, train_sequences = module7.extract_stat(benign_input_file, malware_input_file, stat_output, labels)
#print("--------------")
#benign_input_file1, malware_input_file1 = module1.read_file(OUTPUT_TRAIN_FILE), module1.read_file(TRAIN_ATTACK_FILE)
#unique_system_calls1, train_sequences1 = module7.extract_stat(benign_input_file1, malware_input_file1, stat_output, labels)

"""
# sys_labels_file, output_label_file = module1.read_file(SYS_LABELS_FILE), module1.output_file(SYS_CALLS_LABELS)
# module7.get_labels(sys_labels_file, output_label_file)

# label_file, extracted_sys_call_labels = module1.read_file(SYS_CALLS_LABELS), module1.output_file(EXTRACTED_SYS_CALL_LABELS)
# module7.output_features(unique_system_calls, unique_system_calls1, label_file, extracted_sys_call_labels)

counter, common = 0, 0
for unique_call in unique_system_calls:
    if unique_call not in unique_system_calls1:
        counter += 1
    else:
        common += 1
stat_output.write("different unique calls in train data: %i\n" % counter)
stat_output.write("different unique calls in test data: %i\n" % (len(unique_system_calls1) - common))
stat_output.write("common unique calls in both data: %i\n" % common)"""
########################################################################################

# THIS IS CREATING NGRAMS AND ANALYSIS
K_FOLD_CV, NGRAM_LENGTH, VECTOR_LENGTH, NGRAM_CONTAINER_SIZE = 5, 3, 1000, 10000
ngram_output = module1.output_file(NGRAM_TEST_OUTPUT)
kf = KFold(n_splits=K_FOLD_CV, shuffle=True)

# the model can be trained with SGD
model_type1 = linear_model.LogisticRegression(C=1e5)  # this configured correct  solver='saga'
# model_type2 = LinearSVC()
# model_type3 = SVC()
# model_type4 = linear_model.SGDClassifier()
model_type5 = RandomForestClassifier(n_estimators=1000, max_depth=None, min_samples_split=2, bootstrap=True, oob_score=False)

models = [model_type1, model_type5]  # , model_type2, model_type3, model_type4

#print(math.log(0.0))
#print(math.log(1 + 0.0))
#print(math.log(2 + 0.0))  # 0.69

module7.run_experiment(train_sequences, kf, models, NGRAM_CONTAINER_SIZE, VECTOR_LENGTH, NGRAM_LENGTH, ngram_output)

########################################################################################
benign_input_file, malware_input_file = module1.read_file(OUTPUT_TEST_FILE), module1.read_file(TEST_ATTACK_FILE)
test_unique_system_calls, test_sequences = module7.extract_stat(benign_input_file, malware_input_file, stat_output, labels)

labels = module1.read_file(EXTRACTED_SYS_CALL_LABELS)
module7.test_on_new_data(train_sequences, test_sequences, models, NGRAM_CONTAINER_SIZE, VECTOR_LENGTH, NGRAM_LENGTH, ngram_output, labels)

"""
n_list, accuracies = list(range(1, 11)), []  # for n-gram size
n_list = [100, 500, 1000, 5000, 10000]  #, 50000, 100000, 500000, 1000000]  # for vector length
n_list = [100, 500, 1000, 1500, 2000, 5000]  #, 10000, 100000]  # for trees

for n in n_list:
    model_type5 = RandomForestClassifier(n_estimators=n, max_depth=None, min_samples_split=2, bootstrap=True, oob_score=False)
    models = [model_type5]
    accuracies.append(module7.test_on_new_data(train_sequences, test_sequences, models, NGRAM_CONTAINER_SIZE, VECTOR_LENGTH, NGRAM_LENGTH, ngram_output))
########################################################################################
# module8.plot_two_columns(n_list, accuracies, "k_vs_f-score", "vector length", "f-scores")
module8.plot_two_columns(n_list, accuracies, "trees_vs_f-score", "number of trees", "f-scores")"""
