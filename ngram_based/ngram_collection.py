import math
from random import randint
from textblob import TextBlob
from operator import itemgetter
from sklearn.metrics import f1_score
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

# import nltk
# nltk.download()

from performance_measuring import accuracy_metrics


def build_class_vectors(all_class_vectors, ngram_length, vector_length, container_size):

    ngrams_vectors, unique_ngram_counter = set(), 0

    """for line_index in all_class_vectors:
        local_ngrams = build_ngrams(line_index[1], ngram_length)
        for ngram in local_ngrams:
            ngrams_vectors.add(ngram)
    print("\nTotal number of unique n-grams: %i" % len(ngrams_vectors))
    ngrams = [2274, 9368, 22456, 39565, 57814, 75997, 93007, 108956, 124103]
    length = [2, 3, 4, 5, 6, 7, 8, 9, 10]
    accuracy_metrics.plot_two_columns(length, ngrams, "n_vs_ngrams", "n length", "unique n-grams")"""

    ngrams_vectors, unique_ngram_counter = {}, 0

    for line_index in all_class_vectors:

        local_ngrams = build_ngrams(line_index[1], ngram_length)

        for ngram in local_ngrams:

            if ngram not in ngrams_vectors:
                ngrams_vectors[ngram] = local_ngrams[ngram]

                # maintaining container size limit
                unique_ngram_counter += 1
                if unique_ngram_counter > container_size * 2:  # when the container gets twice bigger
                    sorted_ngrams_vectors = sorted(ngrams_vectors.items(), key=itemgetter(1), reverse=True)
                    ngrams_vectors = {}
                    for item in (sorted_ngrams_vectors[:container_size]):
                        ngrams_vectors[item[0]] = item[1]
                    unique_ngram_counter = 0

            else:
                ngrams_vectors[ngram] += local_ngrams[ngram]

    # sort and get most frequent ngrams
    sorted_ngrams = sorted(ngrams_vectors.items(), key=itemgetter(1), reverse=True)[:vector_length]
    return sorted_ngrams


def build_class_vectors_with_tf_idf(all_class_vectors, ngram_length, vector_length):

    # each row is a system call sequence and each column is unique system call

    # TODO: tf-idf of unique systems calls or unique ngrams?

    unique_ngrams = all_unique_idfs(all_class_vectors, ngram_length)

    unique_ngrams_with_weights = {}
    for index, unique_ngram in enumerate(unique_ngrams):
        item = unique_ngrams[unique_ngram]
        unique_ngrams_with_weights[unique_ngram] = item[0] * item[1]

    # TODO: need to select tf-idf of unique calls
    # return unique_ngrams_with_weights
    sorted_ngrams = sorted(unique_ngrams_with_weights.items(), key=itemgetter(1), reverse=True)[:vector_length]
    return sorted_ngrams


def build_ngrams(sequence, ngram_length):

    local_ngrams, text = {}, TextBlob(sequence)
    ngrams = text.ngrams(n=ngram_length)

    for ngram in ngrams:
        temp_list = []
        [temp_list.append(int(ngram[i])) for i in range(ngram_length)]
        sorted_list = sorted(temp_list)  # sort numerically or alphabetically
        ngram_tuple = tuple(sorted_list)
        local_ngrams[ngram_tuple] = local_ngrams[ngram_tuple] + 1 if ngram_tuple in local_ngrams else 1

    return local_ngrams


def all_unique_idfs(all_class_vectors, ngram_length):

    """train_set = ("The sky is blue.", "The sun is bright.")
    test_set = ("The sun in the sky is bright.",
                "We can see the shining sun, the bright sun.")
    count_vectorizer = CountVectorizer()
    count_vectorizer.fit_transform(train_set)
    print("Vocabulary:", count_vectorizer.vocabulary)
    # Vocabulary: {'blue': 0, 'sun': 1, 'bright': 2, 'sky': 3}
    freq_term_matrix = count_vectorizer.transform(test_set)
    print(freq_term_matrix.todense())

    tfidf = TfidfTransformer(norm="l2")
    tfidf.fit(freq_term_matrix)
    print("IDF:", tfidf.idf_)

    tf_idf_matrix = tfidf.transform(freq_term_matrix)
    print(tf_idf_matrix.todense())"""

    unique_ngrams = {}
    for line_index in all_class_vectors:
        local_ngrams = build_ngrams(line_index[1], ngram_length)
        for ngram in local_ngrams:

            if ngram not in unique_ngrams:
                unique_ngrams[ngram] = 1
            else:
                unique_ngrams[ngram] += 1

    # frequent_unique_ngrams = sorted(unique_ngrams.items(), key=itemgetter(1), reverse=True)
    frequent_unique_ngrams = []
    for ngram in unique_ngrams:
        frequent_unique_ngrams.append((ngram, unique_ngrams[ngram]))

    ngrams_vectors = []

    for line_index in all_class_vectors:

        local_ngrams = build_ngrams(line_index[1], ngram_length)

        row = []
        for item in frequent_unique_ngrams:  # unique_system_calls
            if item[0] not in local_ngrams:
                row.append(0)
            else:
                row.append(local_ngrams[item[0]])
        ngrams_vectors.append(row)

    # transformer = TfidfTransformer(norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False)
    transformer = TfidfTransformer(norm=None, use_idf=True, smooth_idf=True, sublinear_tf=False)
    # tf_idf = transformer.fit_transform(ngrams_vectors).toarray()  # TODO: this is still sparse
    tf_idf = transformer.fit(ngrams_vectors)
    weights = transformer.idf_  # TODO: more weight, more valuable and rare
    # print(weights[:10])
    # tf_idf.get_params()

    """tf_idf_matrix = tf_idf.transform(ngrams_vectors)
    matrix = tf_idf_matrix.todense()  # https://docs.scipy.org/doc/scipy-0.19.0/reference/generated/scipy.sparse.csr_matrix.todense.html
    print(matrix[0][:10])"""

    unique_ngrams_with_weights = {}
    for index, unique_ngram in enumerate(frequent_unique_ngrams):
        unique_ngrams_with_weights[unique_ngram[0]] = (unique_ngram[1], weights[index])

    return unique_ngrams_with_weights
