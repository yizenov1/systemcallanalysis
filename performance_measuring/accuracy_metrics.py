import numpy as np
from sklearn import metrics
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import os


def roc_curve(y_true, y_scores):

    """y = np.array([1, 1, 2, 2])
    scores = np.array([0.1, 0.4, 0.35, 0.8])
    fpr, tpr, thresholds = metrics.roc_curve(y, scores, pos_label=2)
    print(fpr)
    print(tpr)
    print(thresholds)"""

    print("ROC AUC Score: " + str(roc_auc_score(y_true, y_scores)))


def pr_curve():
    print("1")


def roc_auc_plot(fpr, tpr, roc_auc, index):

    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    full_file_path = cur_path + "\\plots\\" + "roc_figure" + str(index) + ".png"

    plt.figure()
    lw = 2  # line width
    plt.plot(fpr, tpr, color='darkorange', lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    try:
        os.remove(full_file_path)
    except OSError:
        pass
    plt.savefig(full_file_path)
    plt.close()


def pr_score_plot(precision, recall, average_precision, index):

    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    full_file_path = cur_path + "\\plots\\" + "pr_figure" + str(index) + ".png"

    plt.figure()
    plt.step(recall, precision, color='b', alpha=0.2, where='post')
    plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.0])
    plt.xlim([0.0, 1.05])
    plt.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(average_precision))
    try:
        os.remove(full_file_path)
    except OSError:
        pass
    plt.savefig(full_file_path)
    plt.close()


def plot_two_columns(column1, column2, file_name, x_label, y_label):

    folder_path = "\\plots\\"
    cur_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
    full_folder_path = cur_path + folder_path

    plt.plot(column1, column2, lw=2)  #.get_figure()
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    file_name = full_folder_path + str(file_name) + ".png"
    try:
        os.remove(file_name)
    except OSError:
        pass
    plt.savefig(file_name)
    plt.close()

